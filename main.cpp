/* Julianna Yee, main.cpp

This file contains the driver program for Puzzle.h
It creates two puzzles, one consisting of ints and one consisting of chars.
It then displays it for the user.

02/23/16 */

#include <iostream>
#include "Puzzle.h"

using namespace std;

int main() {

  Puzzle<int> sudoku; // instantiate Puzzle class with int
  Puzzle<char> wordoku; // instantiate Puzzle class with char
  
  cout << "Sudoku:" << endl << endl;
  sudoku.display(); // display sudoku board
  cout << endl;

  cout << "Wordoku:" << endl << endl;
  wordoku.display(); // display wordoku board

}
