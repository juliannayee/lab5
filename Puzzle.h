/* Julianna Yee, Puzzle.h

This is the interface (.h) file for the templated class Puzzle.
It also includes the implementation file which usually is created in Puzzle.cpp

02/17/16 */

#include <iostream>
#include <vector>
#include <fstream>

using namespace std;

template <typename T>
class Puzzle {
  public:
    Puzzle(); // constructor generates appropriate board
    void display(); // display board values
    void play(); // function to invoke playing sudoku
  private:
    vector< vector<T> > Sudoku; // have two boards to compare numbers
    vector< vector<T> > playSudoku; // that were in the original puzzle 
    int row; // for user input
    int col; // for user input
    int num; // for user input
    bool checkWin(); // helper function to determine whether board is completely filled out or not, return 0 if incomplete, 1 if complete
    bool checkClear(); // helper function to check row/col/minigrid is clear, return 0 if not clear, 1 if clear
    bool checkMiniGrid(); // helper function checks minigrid specifically, return 0 if not clear, 1 if clear
};

template <typename T>
Puzzle<T>::Puzzle() {
  vector<T> tempVec;
  T tempVar;

  string fileName;
  cout << "Please enter a file name: ";
  cin >> fileName;

  ifstream inFile;
  inFile.open(fileName.c_str()); // transform C++ style string to C string

  while(!inFile.eof()) {
    for (int i = 0; i < 9; i++) {
      for (int j = 0; j < 9; j++) {
	inFile >> tempVar;
	tempVec.push_back(tempVar); // insert value one at a time
      }
      Sudoku.push_back(tempVec); // load tempVec values into Sudoku 2D vect
      playSudoku.push_back(tempVec);
      tempVec.clear(); // empty tempVec to prepare adding next row
    }
  }
  row = 0; col = 0; num = 0; // initialize default variables
}

template <typename T>
void Puzzle<T>::display() {
  for (int i = 0; i < 9; i++) {
    if (i % 3 == 0 && i != 0) // print new line every 3 rows
      cout << endl;
    for (int j = 0; j < 9; j++) {
      if (j % 3 == 0  && j != 0) // print space every 3 char/int
	cout << " ";
      cout << playSudoku[i][j] << " ";
    }
    cout << endl; // next row
  }
}

template <typename T>
void Puzzle<T>::play() {
  cout << "Let's play Sudoku!" << endl;
  display(); // display current board

  while (!checkWin()) { // while checkWin == 0
    cout << "Please enter your desired row (1-9): ";  // request user input (row)
    cin >> row; row = row - 1; // must make one less for bounds
    cout << "Please enter your desired column (1-9): "; // request user input (col)
    cin >> col; col = col - 1; // must make one less for bounds

    while (Sudoku[row][col] != 0 || col > 8 || col < 0 || row > 8 || row < 0) { // row/col already has a number, row/col entry invalid
      if (col > 8 || col < 0 || row > 8 || row < 0) // print appropriate error message
	cout << "Error, your entry is out of bounds." << endl;
      else // print appropriate error message
	cout << "Error, you can't change that spot!" << endl;
      cout << "Please enter another row (1-9): "; 
      cin >> row; row = row - 1; // must make one less for bounds
      cout << "Please enter another column (1-9): ";
      cin >> col; col = col - 1; // must make one less for bounds
    } // once while loop breaks, have an empty cell to place a number
    
    cout << "Please enter a number (1-9): ";
    cin >> num;
    
    while (num < 0 || num > 9) { // get appropriate number
      cout << "Error, please enter a number (1-9): ";
      cin >> num;
    } 
    
    if (checkClear() == 1) { // row/col/minigrid do not have the number
      playSudoku[row][col] = num; // replace the 0 with the user's number
      display(); // display new puzzle
    }
    else {// otherwise, restart loop
      cout << "Try again!" << endl;
      display(); // redisplay board
    }
  } // close checkWin while loop
  cout << "Wow, you solved the puzzle! You're rad!" << endl;
} // end function

template <typename T>
bool Puzzle<T>::checkWin() {
  for (int i = 0; i < 9; i++) 
    for (int j = 0; j < 9; j++) {
      if (playSudoku[i][j] == 0) 
	return 0; // puzzle not completed, 0(s) still found
    } 
  return 1; // puzzle completed, no 0s!
}

template <typename T>
bool Puzzle<T>::checkClear() {
  for (int i = 0; i < 9; i++) {// check row is clear
    if (Sudoku[row][i] == num) {
      cout << "Sorry, there is already a " << num << " in that row." << endl;
      return 0; // prompt for another #
    }
    else { // row is clear
      for (int j = 0; j < 9; j++) { // check column is clear
	if (Sudoku[i][col] == num) {
	  cout << "Sorry, there is already a " << num << " in that column." << endl;
          return 0; // prompt for another #
	}
        else { // column is clear      
	  if (checkMiniGrid() == 0) {// mini grid is not clear
	    cout << "Sorry, there is already a " << num << " in that mini grid." << endl;
	    return 0; // prompt for another #
	  }
	}
      }
    }
  }
  return 1; // cleared all constraints
}


template <typename T>
bool Puzzle<T>::checkMiniGrid() {

// *** *** ***  MINI
// *1* *2* *3*  GRID
// *** *** ***  LAYOUT
//
// *** *** ***
// *4* *5* *6*
// *** *** ***
//
// *** *** ***
// *7* *8* *9*
// *** *** ***
  
  if (row <= 2) {
    if (col <= 2) { // minigrid 1 
      for (int a = 0; a < 3; a++) {
	for (int b = 0; b < 3; b++) {
	  if (Sudoku[a][b] == num)
	    return 0; // minigrid 1 is not clear
	}
      }
    }
    else if (col > 2 && col <= 5) {// minigrid 2 {
      for (int a = 0; a < 3; a++) {
	for (int b = 3; b < 6; b++) {
	  if (Sudoku[a][b] == num)
	    return 0; // minigrid 2 is not clear
	}
      }
    }
    else { // minigrid 3
      for (int a = 0; a < 3; a++) {
	for (int b = 6; b < 9; b++) {
	  if (Sudoku[a][b] == num)
	    return 0; // minigrid 3 is not clear
	}
      }
    }
  }
  else if (row > 2 && row <= 5) {
    if (col <= 2) { // minigrid 4
      for (int a = 3; a < 6; a++) {
	for (int b = 0; b < 3; b++) {
	  if (Sudoku[a][b] == num)
	    return 0; // minigrid 4 is not clear
	}
      }
    }
    else if (col > 2 && col <= 5) { // minigrid 5
      for (int a = 3; a < 6; a++) {
	for (int b = 3; b < 6; b++) {
	  if (Sudoku[a][b] == num)
	    return 0; // minigrid 5 is not clear
	}
      }
    }
    else { // minigrid 6
      for (int a = 3; a < 6; a++) {
	for (int b = 6; b < 9; b++) {
	  if (Sudoku[a][b] == num)
	    return 0; // minigrid 6 is not clear
	}
      }
    }
  }
  else {
    if (col <= 2) { // minigrid 7 
      for (int a = 6; a < 9; a++) {
	for (int b = 0; b < 3; b++) {
	  if (Sudoku[a][b] == num)
	    return 0; // minigrid 7 is not clear
	}
      }
    } 
    else if (col > 2 && col <= 5) { // minigrid 8
      for (int a = 6; a < 9; a++) {
	for (int b = 3; b < 6; b++) {
	  if (Sudoku[a][b] == num)
	    return 0; // minigrid 8 is not clear
	}
      }
    }
    else { // minigrid 9
      for (int a = 6; a < 9; a++) {
	for (int b = 6; b < 9; b++) {
	  if (Sudoku[a][b] == num)
	    return 0; // minigrid 9 is not clear
	}
      }
    }
  }
  return 1; // the appropriate minigrid is clear
}
