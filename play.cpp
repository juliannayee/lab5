/* Julianna Yee, play.cpp

Driver program to play sudoku for Lab 5 Part 2

02/23/16 */

#include <iostream>
#include "Puzzle.h"

using namespace std;

int main() {
  
  Puzzle<int> sudoku; // instantiate sudoku puzzle
  sudoku.play(); // play sudoku!

}
